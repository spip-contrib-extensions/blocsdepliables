<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/blocsdepliables?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// B
	'bloc_deplier' => 'Unfold',
	'bloc_replier' => 'Fold back',
	'blocsdepliables_titre' => 'Drop-down blocks',

	// E
	'explication_cookie' => 'The state of the numbered blocks will be stored in a cookie during the session, in order to keep the page’s aspect for a subsequent visit.',
	'explication_unique' => 'Opening a block will trigger all the other blocks on the page to close, in order to have one single opened block at the time.',

	// L
	'label_activer_prive_1' => 'Enable drop-down blocks in the private area',
	'label_animation' => 'Animation',
	'label_animation_aucun' => 'None',
	'label_animation_fast' => 'Fast slide',
	'label_animation_millisec' => 'Slide duration :',
	'label_animation_normal' => 'Normal slide',
	'label_animation_slow' => 'Slow slide',
	'label_balise_titre' => 'Tag for the blocks title',
	'label_cookie_1' => 'Remember the blocks state',
	'label_unique_1' => 'A single block opened on the page',

	// P
	'pp_blocs_bloc' => 'Insert a folded block',
	'pp_blocs_visible' => 'Insert an unfolded block',
	'pp_un_titre' => 'A title',
	'pp_votre_texte' => 'Insert your text here',

	// T
	'titre_page_configurer_blocsdepliables' => 'Drop-down blocks',
];
