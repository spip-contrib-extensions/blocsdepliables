<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/blocsdepliables.git

return [

	// B
	'blocsdepliables_description' => 'Vous permet de créer des blocs dont le titre cliquable peut les rendre visibles ou invisibles.',
	'blocsdepliables_nom' => 'Blocs Dépliables',
	'blocsdepliables_slogan' => 'Des blocs dépliables.',
];
