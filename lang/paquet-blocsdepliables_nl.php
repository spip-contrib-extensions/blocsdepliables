<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-blocsdepliables?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// B
	'blocsdepliables_description' => 'Door op de titel te klikken maak je blokken inhoud zichtbaar of onzichtbaar.',
	'blocsdepliables_nom' => 'Openvouwbare Blokken',
	'blocsdepliables_slogan' => 'Openvouwbare blokken.',
];
