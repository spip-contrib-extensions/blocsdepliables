<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/blocsdepliables.git

return [

	// B
	'bloc_deplier' => 'Déplier',
	'bloc_replier' => 'Replier',
	'blocsdepliables_titre' => 'Blocs Dépliables',

	// E
	'explication_cookie' => 'L’état des blocs numérotés sera stocké dans un cookie le temps de la session, afin de conserver l’aspect de la page en cas de retour.',
	'explication_unique' => 'L’ouverture d’un bloc provoquera la fermeture de tous les autres blocs de la page, afin d’en avoir qu’un seul ouvert à la fois.',

	// L
	'label_activer_prive_1' => 'Activer les blocs déroulants dans l’espace privé',
	'label_animation' => 'Animation',
	'label_animation_aucun' => 'Aucune',
	'label_animation_fast' => 'Glissement rapide',
	'label_animation_millisec' => 'Glissement durant :',
	'label_animation_normal' => 'Glissement normal',
	'label_animation_slow' => 'Glissement lent',
	'label_balise_titre' => 'Balise pour les titres des blocs',
	'label_cookie_1' => 'Mémoriser l’état des blocs',
	'label_unique_1' => 'Un seul bloc ouvert sur la page',

	// P
	'pp_blocs_bloc' => 'Insérer un bloc replié',
	'pp_blocs_visible' => 'Insérer un bloc déplié',
	'pp_un_titre' => 'Un titre',
	'pp_votre_texte' => 'Placez votre texte ici',

	// T
	'titre_page_configurer_blocsdepliables' => 'Blocs dépliables',
];
