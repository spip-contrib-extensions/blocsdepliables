<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-blocsdepliables?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// B
	'blocsdepliables_description' => 'Permite criar blocos que se tornam visíveis ou invisíveis ao se clicar no título.',
	'blocsdepliables_nom' => 'Blocos Expansíveis',
	'blocsdepliables_slogan' => 'Blocos expansíveis',
];
