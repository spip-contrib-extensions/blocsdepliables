<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/blocsdepliables?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// B
	'bloc_deplier' => 'Expandir',
	'bloc_replier' => 'Contrair',
	'blocsdepliables_titre' => 'Blocos Expansíveis',

	// E
	'explication_cookie' => 'O status dos blocos numerados será armazenado em cookie pelo tempo da sessão, para conservar a aparência da página, em caso de retorno.',
	'explication_unique' => 'A abertura de um bloco provocará o fechamento de todos os outros blocos da página, mantendo apenas um aberto.',

	// L
	'label_activer_prive_1' => 'Ativar os blocos expansíveis na área restrita',
	'label_animation' => 'Animação',
	'label_animation_aucun' => 'Nenhum',
	'label_animation_fast' => 'Deslizamento rápido',
	'label_animation_millisec' => 'Deslizamento durando:',
	'label_animation_normal' => 'Deslizamento normal',
	'label_animation_slow' => 'Deslizamento lento',
	'label_balise_titre' => 'Tag para os títulos dos blocos',
	'label_cookie_1' => 'Memorizar o status dos blocos',
	'label_unique_1' => 'Um único bloco aberto por página',

	// P
	'pp_blocs_bloc' => 'Inserir um bloco fechado',
	'pp_blocs_visible' => 'Inserir um bloco aberto',
	'pp_un_titre' => 'Um título',
	'pp_votre_texte' => 'Coloque o seu texto aqui',

	// T
	'titre_page_configurer_blocsdepliables' => 'Blocos expansívels',
];
