<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/blocsdepliables?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// B
	'bloc_deplier' => 'Openvouwen',
	'bloc_replier' => 'Samenvouwen',
	'blocsdepliables_titre' => 'Openvouwbare Blokken',

	// E
	'explication_cookie' => 'De status van de genummerde blokken wordt gedurende de sessie in een cookie opgeslagen, zodat de pagina er bij het terugkeren identiek uit ziet.',
	'explication_unique' => 'Het openen van een blok veroorzaakt het sluiten van alle andere blokken op de bladzijde. Er is er dus maar één tegelijk geopend.',

	// L
	'label_animation' => 'Animatie',
	'label_animation_aucun' => 'Geen',
	'label_animation_fast' => 'Snel glijden',
	'label_animation_millisec' => 'Glijden gedurende:',
	'label_animation_normal' => 'Normaal glijden',
	'label_animation_slow' => 'Langzaam glijden',
	'label_balise_titre' => 'Baken voor de titels van de blokken',
	'label_cookie_1' => 'De status van de blokken onthouden',
	'label_unique_1' => 'Eén enkel geopend blok per bladzijde',

	// P
	'pp_blocs_bloc' => 'Een gesloten blok invoegen',
	'pp_blocs_visible' => 'Een opengevouwen blok invoegen',
	'pp_un_titre' => 'Een titel',
	'pp_votre_texte' => 'Voer hier je tekst in',

	// T
	'titre_page_configurer_blocsdepliables' => 'Openvouwbare Blokken',
];
