<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-blocsdepliables?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// B
	'blocsdepliables_description' => 'Create blocks that can be opened or closed by clicking on their titles.',
	'blocsdepliables_nom' => 'Folding Blocks',
	'blocsdepliables_slogan' => 'Folding Blocks.',
];
