# Changelog

## Unreleased

### Changed

- Compatible à partir de SPIP 4.1 seulement

## 1.4.1 - 2024-10-27

### Fixed

- #10 Compatiblité avec les urls arbo, cassé en 1.4.0

## 1.4.0 - 2024-10-20

### Fixed

- #7 Ne plus afficher d'avertissement javascript dans le privé
- #8 Activer les blocs déroulants dans le privé (selon config)

### Added

- Par défaut, les blocs déroulants sont activés dans le privé
- Ajout d'un CHANGELOG

### Removed

- Attribud type='text/javascript'
