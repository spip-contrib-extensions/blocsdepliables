<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function blocsdepliables_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];
	$maj['1.0.0'] = [
		['blocsdepliables_activer_config'],
	];
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

function blocsdepliables_vider_tables($nom_meta_base_version) {
	effacer_meta($nom_meta_base_version);
}

function blocsdepliables_activer_config() {
	include_spip('inc/config');
	ecrire_config('blocsdepliables/activer_prive', 1);
}
